import os
import math
import time

class Display(object):
	def __init__(self, torrent):
		self.torrent = torrent
		self.fm = self.torrent.file_manager
		self.length = self.fm.file_to_stream_length
		self.piece_size = self.fm.piece_length
		self.start_piece = self.fm.start_piece_id
		self.no_of_pieces = int(math.ceil((float)self.length/(float)self.piece_size))
	def DisplayInfo(self):
		while self.fm.StreamCompleted == False:
			time.sleep(3)
			os.system('clear')
			count = 0
			print 'Complete Status : ' + str(count/self.no_of_pieces) + '\n'
			if self.no_of_pieces < 100:
				mapToOne = 1
			else
				mapToOne = int(math.ceil((float)self.no_of_pieces/(float)100))
			for x in xrange(self.start_piece,self.no_of_pieces,mapToOne):	
				intermediateCount = 0
				for y in xrange(x,x+mapToOne):
					no_of_blocks = self.fm.getBlockCountFor(y)
					flag = true
					for i in xrange(no_of_blocks):
						if self.fm.blockExists(y,i) == False:
							flag = false
							break
					if flag == True:
						intermediateCount += 1
				if intermediateCount == mapToOne:
					print '|'
				else if intermediateCount > 0:
					print '!'
				else:
					print '.'



		
		